[1]A. H. C. Ng, B. B. Li, M. D. Chamberlain, and A. R. Wheeler, “Digital Microfluidic Cell Culture,” Annu. Rev. Biomed. Eng., vol. 17, no. 1, pp. 91–112, Dec. 2015, doi: 10.1146/annurev-bioeng-071114-040808.

[2]W. Shi, L. Guo, H. Kasdan, and Y.-C. Tai, “Four-part leukocyte differential count based on sheathless microflow cytometer and fluorescent dye assay,” Lab Chip, vol. 13, no. 7, p. 1257, 2013, doi: 10.1039/c3lc41059e.

[3]V. Lecault et al., “High-throughput analysis of single hematopoietic stem cell proliferation in microfluidic cell culture arrays,” Nat Methods, vol. 8, no. 7, pp. 581–586, Jul. 2011, doi: 10.1038/nmeth.1614.

[4]R. A. Kellogg, R. Gómez-Sjöberg, A. A. Leyrat, and S. Tay, “High-throughput microfluidic single-cell analysis pipeline for studies of signaling dynamics,” Nat Protoc, vol. 9, no. 7, pp. 1713–1726, Jul. 2014, doi: 10.1038/nprot.2014.120.

[5]A. J. Kaestli, M. Junkin, and S. Tay, “Integrated platform for cell culture and dynamic quantification of cell secretion,” Lab Chip, vol. 17, no. 23, pp. 4124–4133, 2017, doi: 10.1039/C7LC00839B.

[6]M. Mehling and S. Tay, “Microfluidic cell culture,” Current Opinion in Biotechnology, vol. 25, pp. 95–102, Feb. 2014, doi: 10.1016/j.copbio.2013.10.005.

[7]M.-H. Wu, S.-B. Huang, and G.-B. Lee, “Microfluidic cell culture systems for drug research,” Lab Chip, vol. 10, no. 8, p. 939, 2010, doi: 10.1039/b921695b.

[8]M. A. Unger, “Monolithic Microfabricated Valves and Pumps by Multilayer Soft Lithography,” Science, vol. 288, no. 5463, pp. 113–116, Apr. 2000, doi: 10.1126/science.288.5463.113.

[9]B. G. Wong, C. P. Mancuso, S. Kiriakov, C. J. Bashor, and A. S. Khalil, “Precise, automated control of conditions for high-throughput growth of yeast and bacteria with eVOLVER,” Nat Biotechnol, vol. 36, no. 7, pp. 614–623, Aug. 2018, doi: 10.1038/nbt.4151.

[10]R. Gómez-Sjöberg, A. A. Leyrat, D. M. Pirone, C. S. Chen, and S. R. Quake, “Versatile, Fully Automated, Microfluidic Cell Culture System,” Anal. Chem., vol. 79, no. 22, pp. 8557–8563, Nov. 2007, doi: 10.1021/ac071311w.

